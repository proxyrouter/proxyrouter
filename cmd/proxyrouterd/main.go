package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"log"
	"net"
	"os"
	"strconv"
	"time"
)

const (
	socksVer = 0x05

	authTypeNoAuthRequired      = 0x00
	authTypeLoginPass           = 0x02
	authTypeNoAcceptableMethods = 0xFF

	repSucceeded            = 0x00
	repGeneralServerFailure = 0x01

	authVer     = 0x01
	authSuccess = 0x00
	authFailure = 0x01

	cmdConnect = 0x01

	addressTypeIPv4       byte = 0x01
	addressTypeDomainName byte = 0x03
	addressTypeIPv6       byte = 0x04

	portLenBytes = 2
)

const (
	connKeepAlivePeriod time.Duration = time.Second * 15
	connTimeout         time.Duration = time.Second * 5
	connDeadline        time.Duration = time.Hour
)

//nolint:gochecknoglobals
var (
	verbose      bool
	extraVerbose bool
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	addrSocks := os.Getenv("LISTEN_SOCKS")
	if addrSocks == "" {
		addrSocks = "127.0.0.1:1080"
	}

	flag.StringVar(&addrSocks, "listen-socks", addrSocks, "адрес и порт, на котором будет висеть SOCKS5 прокси")
	flag.BoolVar(&verbose, "v", verbose, "включаем лог ошибок соединения")
	flag.BoolVar(&extraVerbose, "vv", extraVerbose, "включаем лог соединений")
	flag.Parse()

	host, port, err := net.SplitHostPort(addrSocks)
	if err != nil {
		log.Fatalln(err)
	}

	port2, err := strconv.ParseUint(port, 10, 16)
	if err != nil {
		log.Fatalln(err)
	}

	//nolint:exhaustruct
	laddr := &net.TCPAddr{
		IP:   net.ParseIP(host),
		Port: int(port2),
	}

	tcpListener, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		log.Fatalln(err)
	}

	if verbose {
		log.Println("слушаем на", laddr.String())
	}

	defer func() {
		err = tcpListener.Close()
		if err != nil {
			log.Println(err)
		}
	}()

	for {
		conn, err := tcpListener.AcceptTCP()
		if err != nil {
			log.Println(err)

			return
		}

		go process(conn)
	}
}

//nolint:funlen,gocognit,cyclop,gocyclo,maintidx
func process(conn *net.TCPConn) {
	defer func() {
		_ = conn.Close()
	}()

	err := conn.SetKeepAlive(true)
	if err != nil {
		log.Println(err)
	}

	err = conn.SetKeepAlivePeriod(connKeepAlivePeriod)
	if err != nil {
		log.Println(err)
	}

	err = conn.SetReadDeadline(time.Now().Add(connTimeout))
	if err != nil {
		log.Println(err)
	}

	err = conn.SetWriteDeadline(time.Now().Add(connTimeout))
	if err != nil {
		log.Println(err)
	}

	// буфер, который будем всячески переиспользовать. длина 255 - чтоб можно было не заморачиваться на доп проверки
	buf := make([]byte, 0, 255) //nolint:gomnd

	// читаем первые два байта.
	bufLen := 2
	buf = buf[:bufLen]

	if n, err := conn.Read(buf); err != nil || n < bufLen {
		// если при чтении возникла ошибка - просто закрываем соединение
		return
	}

	// Версия всегда должна быть 5
	if buf[0] != socksVer {
		return
	}

	// Читаем поддерживаемые варианты аутентификации.
	bufLen = int(buf[1])
	buf = buf[:bufLen]

	if _, err := conn.Read(buf); err != nil {
		return
	}

	// Нет поддерживаемых типов авторизации
	if bytes.IndexByte(buf, authTypeLoginPass) == -1 {
		response := []byte{socksVer, authTypeNoAcceptableMethods}
		if n, err := conn.Write(response); err != nil || n < len(response) {
			return
		}

		return
	}

	// Запрашиваем аутентификацию по паролю
	response := []byte{socksVer, authTypeLoginPass}
	if n, err := conn.Write(response); err != nil || n < len(response) {
		return
	}

	// Читаем первые два байта запроса с логином/паролем
	bufLen = 2
	buf = buf[:bufLen]

	if n, err := conn.Read(buf); err != nil || n < bufLen {
		return
	}

	// Проверяем версию протокола аутентификации и длину логина
	if buf[0] != authVer {
		return
	}

	// Читаем логин
	bufLen = int(buf[1])
	buf = buf[:bufLen]

	if n, err := conn.Read(buf); err != nil || n < bufLen {
		return
	}

	usrLogin := string(buf)

	// Читаем длину пароля
	bufLen = 1
	buf = buf[:bufLen]

	if n, err := conn.Read(buf); err != nil || n < bufLen {
		return
	}

	// Читаем пароль
	bufLen = int(buf[0])
	buf = buf[:bufLen]

	if n, err := conn.Read(buf); err != nil || n < bufLen {
		return
	}

	usrPassword := string(buf)

	// Проверяем, что в качестве логина указан корректный IP адрес.
	proxyIP := net.ParseIP(usrLogin)
	if proxyIP == nil {
		response := []byte{authVer, authFailure}
		if _, err := conn.Write(response); err != nil {
			return
		}

		return
	}

	// Проверяем, что в качестве пароля указан корректный порт
	proxyPort, err := strconv.ParseUint(usrPassword, 10, 16)
	if err != nil || proxyPort < 1 {
		response := []byte{authVer, authFailure}
		if _, err := conn.Write(response); err != nil {
			return
		}

		return
	}

	// Сообщяем, что авторизация прошла успешно
	response = []byte{authVer, authSuccess}
	if _, err := conn.Write(response); err != nil {
		return
	}

	// Читаем первые 4 байт запроса
	bufLen = 4
	buf = buf[:bufLen]

	if n, err := conn.Read(buf); err != nil || n < bufLen {
		return
	}

	// Проверяем версию протокола
	if buf[0] != socksVer {
		return
	}

	// Проверяем метод
	if buf[1] != cmdConnect {
		log.Println(conn.RemoteAddr().String(), "неподдерживаемый метод", buf[1])

		return
	}

	req := make([]byte, 0, bufLen)
	req = append(req, buf...)

	// Проверяем тип адреса
	switch buf[3] {
	case addressTypeIPv4:
		// Читаем 4 байта IPv4-адреса и 2 байта порта
		bufLen = net.IPv4len + portLenBytes
		buf = buf[:bufLen]

		if n, err := conn.Read(buf); err != nil || n < bufLen {
			return
		}

		if verbose {
			log.Println("запрос", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword),
				"IPv4", net.IPv4(buf[0], buf[1], buf[2], buf[3]).String(), binary.BigEndian.Uint16(buf[4:]))
		}

	case addressTypeDomainName:
		// Читаем длину адреса
		bufLen = 1
		buf = buf[:bufLen]

		if n, err := conn.Read(buf); err != nil || n < bufLen {
			return
		}

		req = append(req, buf...)

		// Читаем адрес + 2 байта порт
		bufLen = int(buf[0]) + portLenBytes
		buf = buf[:bufLen]

		if n, err := conn.Read(buf); err != nil || n < bufLen {
			return
		}

		if extraVerbose {
			log.Println("запрос", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword),
				"domain", string(buf[:len(buf)-2]), binary.BigEndian.Uint16(buf[len(buf)-2:]))
		}

	case addressTypeIPv6:
		// Читаем 16 байта IPv6-адреса 2 байта порта
		bufLen = net.IPv6len + portLenBytes
		buf = buf[:bufLen]

		if n, err := conn.Read(buf); err != nil || n < bufLen {
			return
		}

		if verbose {
			log.Println("запрос", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword),
				"IPv6", (net.IP)(buf[:16]).String(), binary.BigEndian.Uint16(buf[16:]))
		}

	default:
		log.Println(conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword),
			"неподдерживаемый тип адреса", buf[3])

		return
	}

	req = append(req, buf...)

	// Подключаемся к проксе

	deadline := time.Now().Add(connDeadline)
	_ = conn.SetDeadline(deadline)
	_ = conn.SetReadDeadline(deadline)
	_ = conn.SetWriteDeadline(deadline)

	//nolint:exhaustruct
	laddr := &net.TCPAddr{}

	//nolint:exhaustruct
	raddr := &net.TCPAddr{
		IP:   proxyIP,
		Port: int(proxyPort),
	}

	proxyConn, err := net.DialTCP("tcp", laddr, raddr)
	if err != nil {
		if verbose {
			log.Println("ошибка", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), err)
		}

		response = []byte{socksVer, repGeneralServerFailure}
		response = append(response, req[2:]...)
		_, _ = conn.Write(response)

		return
	}

	defer func() {
		_ = proxyConn.Close()
	}()

	_ = proxyConn.SetKeepAlive(true)
	_ = proxyConn.SetKeepAlivePeriod(connKeepAlivePeriod)
	_ = proxyConn.SetWriteDeadline(time.Now().Add(connTimeout))
	_ = proxyConn.SetReadDeadline(time.Now().Add(connTimeout))

	// отправляем заголовки

	response = []byte{socksVer, 0x01, authTypeNoAuthRequired}
	if n, err := proxyConn.Write(response); err != nil || n < len(response) {
		if verbose {
			log.Println("ошибка", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), err)
		}

		response = []byte{socksVer, repGeneralServerFailure}
		response = append(response, req[2:]...)
		_, _ = conn.Write(response)

		return
	}

	// Читаем авторизацию
	bufLen = 2
	buf = buf[:bufLen]

	if n, err := proxyConn.Read(buf); err != nil || n < bufLen {
		if verbose {
			log.Println("ошибка", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), err)
		}

		response = []byte{socksVer, repGeneralServerFailure}
		response = append(response, req[2:]...)
		_, _ = conn.Write(response)

		return
	}

	if buf[0] != socksVer || buf[1] != authSuccess {
		if verbose {
			log.Println("ошибка авторизации", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), buf)
		}

		response = []byte{socksVer, repGeneralServerFailure}
		response = append(response, req[2:]...)
		_, _ = conn.Write(response)

		return
	}

	if extraVerbose {
		log.Println("отправляем запрос на проксю",
			conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), req)
	}

	if n, err := proxyConn.Write(req); err != nil || n < len(req) {
		if verbose {
			log.Println("ошибка авторизации", conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), err)
		}

		response = []byte{socksVer, repGeneralServerFailure}
		response = append(response, req[2:]...)
		_, _ = conn.Write(response)

		return
	}

	// работаем

	_ = proxyConn.SetDeadline(deadline)
	_ = proxyConn.SetReadDeadline(deadline)
	_ = proxyConn.SetWriteDeadline(deadline)

	go func() {
		n, err := proxyConn.ReadFrom(conn)
		if extraVerbose {
			log.Println(conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), "закрываем аплинк", n, err)
		}

		_ = proxyConn.Close()
		_ = conn.Close()
	}()

	n, err := conn.ReadFrom(proxyConn)
	if extraVerbose {
		log.Println(conn.RemoteAddr().String(), "->", net.JoinHostPort(usrLogin, usrPassword), "закрываем даунлинк", n, err)
	}
}
