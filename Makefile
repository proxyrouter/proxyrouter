.PHONY: snapshot release lint

snapshot:
	goreleaser release --snapshot --rm-dist

release:
	goreleaser release --rm-dist

lint:
	golangci-lint run
